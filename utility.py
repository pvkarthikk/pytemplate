import subprocess,logging,os,sys,time,csv

# It allows to print ASCII in terminal
# @https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-terminal-in-python
os.system("")
# TODO function override protection needs to be supported
class Utility:
    def __init__(self,filepath=None,debug=False):
        debugfilepath = os.getcwd()+"\\"+str(os.path.basename(__file__))+".log"
        if(filepath != None):
            debugfilepath = filepath
        logging.basicConfig(
            filename=debugfilepath,
            filemode='w', 
            format='%(asctime)s - %(filename)s - %(lineno)d - %(levelname)s - %(message)s',
            )
        self.__log = logging.getLogger()
        if(debug):
            self.__log.setLevel(logging.DEBUG)
        else:
            self.__log.setLevel(logging.INFO)
        pass
    def debug(self,msg):
        """logs the message """
        self.__log.debug(msg)
        pass
    def info(self,msg):
        """write the message to output and logs"""
        self.__log.info(msg)        
        self.output(msg)
        pass
    def success(self,msg):
        """Alert warning and logs the message """
        self.__log.info(msg)
        self.output('\033[32mSUCCESS\x1b[0m\t:{0}'.format(msg))
        pass
    def warning(self,msg):
        """Alert warning and logs the message """
        self.__log.warning(msg)
        self.output('\033[33mWARNING\x1b[0m\t:{0}'.format(msg))
        pass
    def error(self,msg):
        """Alert error,logs the message and exit"""
        self.__log.error(msg)
        self.output('\033[31mERROR\x1b[0m\t:{0}'.format(msg))
        sys.exit(1)
        pass
    def csvparser(self,csvpath):
        """Reads csv and return as list"""
        output = []
        self.debug(csvpath)
        if(not os.path.exists(csvpath)):
            self.warning("Invalid file path : {0}".format(csvpath))
        else:
            with open(csvpath, 'r') as csvfile:
                csvreader = csv.reader(csvfile)
                for row in csvreader:
                    output.append(row)
        return output
        pass
    def output(self,msg):
        """Write output to the command prompt"""
        sys.stdout.write('{}\r\n'.format(msg))
        pass
    def generate(self,path,content):
        """Helps to generate file"""
        self.debug(path)
        self.debug(type(content))
        if(not os.path.exists(os.path.dirname(path))):
            self.warning("Invalid file path : {0}".format(path))
        file = open(path,'w')        
        if isinstance(content,list):
            for line in content:
                file.write(line+'\n')
            pass
        else:
            file.write(content)
        file.close()
        pass
    def where(self,tool):
        """Check 3rd party tools with console output"""
        exe = ["where",tool]
        out,err = self.run(exe)
        # Check for error message
        flag =  True  if(len(err) == 0 or err == None) else False
        return flag
        pass
    def run(self,exe,wd=None):        
        """Runs the third party exe and catpure outputs/errors"""   
        if(wd == None):
            wd = os.getcwd()
        self.debug("{} {}".format(wd,exe))
        cmd = subprocess.Popen(exe,cwd=wd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
        #cmd.wait()
        out,err = cmd.communicate()
        self.debug("Output: {}".format(out))
        self.debug("Error : {}".format(err))
        return out,err
        pass
    pass

if __name__ == "__main__":
    utils  = Utility(debug=True)
    utils.output("init")
    utils.where("git")
    utils.where("git1")
    utils.output("end")
    pass
