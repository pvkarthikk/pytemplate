#!/usr/bin/env python
from utility import Utility,sys,os
import time
class Application():
    def __init__(self,sys_argv):
        self.utils = Utility(filepath=os.getcwd()+"\\"+str(os.path.basename(__file__))+".log")
        self.args = sys_argv
        pass
    def run(self):
        self.utils.output(self.args)
        self.utils.debug("debug test")
        self.utils.info("info test")
        self.utils.success("success test")
        self.utils.warning("warning test")
        self.utils.output("output test")
        self.utils.where("git")
        self.utils.run("git")
        while True:
            time.sleep(1.0)
            self.utils.info("In loop... Ctrl+C to break it")
            pass
        pass
    def exit(self):
        self.utils.error("error test")
        pass
    pass

if __name__ == "__main__":
    app = Application(sys.argv)
    try:
        app.run()
    except:
        app.exit()
    pass
